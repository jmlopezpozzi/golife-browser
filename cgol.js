/* Conway's Game of Life */
/* Implementation by Juanmi Lopez */

"use strict";


/*   Type conventions:
 * uIGrid: HTML table containing a checkbox (<input type="checkbox"> element)
 *   in each <td> element.
 * stateGrid: 2D array (as an array of arrays) of booleans.
 */


/* function createUIGrid(rows, columns)
 * Creates and returns a uIGrid with dimensions given by @rows and @columns.
 */
function createUIGrid(rows, columns) {
	let table = document.createElement("table");

	for (let j = 0; j < rows; j += 1) {
		let tr = document.createElement("tr");

		for (let i = 0; i < columns; i += 1) {
			let td = document.createElement("td");
			let checkbox = document.createElement("input");

			checkbox.type = "checkbox";
			td.appendChild(checkbox);
			tr.appendChild(td);
		}

		table.appendChild(tr);
	}

	return table;
}


/* function generateStateRandom(rows, columns)
 * Creates and returns a stateGrid with dimensions given by @rows and @columns
 * and with each element filled randomly.
 */
function generateStateRandom(rows, columns) {
	let stateGrid = [];

	for (let j = 0; j < rows; j += 1) {
		let stateGridRow = [];

		for (let i = 0; i < columns; i += 1) {
			let isAlive = false;
			if (Math.random() > 0.5) isAlive = true;

			stateGridRow.push(isAlive);
		}

		stateGrid.push(stateGridRow);
	}

	return stateGrid;
}


/* function generateStateClear(rows, columns)
 * Creates and returns a stateGrid with dimensions given by @rows and @columns
 * and with each element filled with false.
 */
function generateStateClear(rows, columns) {
	let stateGrid = [];

	for (let j = 0; j < rows; j += 1) {
		let stateGridRow = [];

		for (let i = 0; i < columns; i += 1) {
			stateGridRow.push(false);
		}

		stateGrid.push(stateGridRow);
	}

	return stateGrid;
}


/* function generateStateFromUI(uIGrid)
 * Returns a stateGrid with its elements representing the state of each
 * corresponding checkbox in @uIGrid.
 */
function generateStateFromUI(uIGrid) {
	let stateGrid = [];
	let rows = uIGrid.getElementsByTagName("tr");  // This function returns a live collection (we're not adding or deleting nodes anyway)

	for (let j = 0; j < rows.length; j += 1) {
		let stateRow = [];
		let columns = rows[j].getElementsByTagName("td");

		for (let i = 0; i < columns.length; i += 1) {
			let checkbox = columns[i].firstChild;  // The first child of each <td> element is an <input type="checkbox"> as created by createUIGrid
			stateRow.push(checkbox.checked);
		}

		stateGrid.push(stateRow);
	}

	return stateGrid;
}


/* function simulateLife(stateGrid, wrapAround)
 * Returns a stateGrid representing the result of applying the Conway's Game of
 * Life rules on the provided @stateGrid. @wrapAround is a boolean that, when
 * true, makes the simulation wrap around the edges of the grid when set.
 */
function simulateLife(stateGrid, wrapAround) {
	let newState = [];
	let rows = stateGrid.length;
	let cols = stateGrid[0].length;
	let countNeighbours = wrapAround ? countNeighboursWrap :
	                                   countNeighboursLimit;

	for (let j = 0; j < rows; j += 1) {
		let newStateRow = [];

		for (let i = 0; i < cols; i += 1) {
			let wasAlive = stateGrid[j][i];
			let isAlive = false;
			let neighbours = countNeighbours(j, i);

			if (wasAlive && (neighbours > 1 && neighbours < 4) ||
			    !wasAlive && neighbours == 3)
			{
				isAlive = true;
			}
			newStateRow.push(isAlive);
		}

		newState.push(newStateRow);
	}

	return newState;

	/* function countNeighboursLimit(j, i)
	 * Counts the neighbours of the cell in row @j and column @i of the grid.
	 */
	function countNeighboursLimit(j, i) {
		let jStart = Math.max(0, j - 1);
		let iStart = Math.max(0, i - 1);
		let jEnd = Math.min(rows, j + 2);
		let iEnd = Math.min(cols, i + 2);
		let neighbours = 0;

		for (let curJ = jStart; curJ < jEnd; curJ += 1) {
			for (let curI = iStart; curI < iEnd; curI += 1) {
				if (stateGrid[curJ][curI]) neighbours += 1;
			}
		}

		if (stateGrid[j][i]) neighbours -= 1;  // Substract local cell if it was counted as a neighbour

		return neighbours;
	}

	/* function countNeighboursWrap(j, i)
	 * Counts the neighbours of the cell in row @j and column @i of the grid,
	 * considering elements in borders to be adjacent to elements of the
	 * opposite borders (NOTE: Minimum grid size has to be 3 * 3).
	 */
	function countNeighboursWrap(j, i) {
		let jStart = j - 1;
		let iStart = i - 1;
		let neighbours = 0;

		if (jStart < 0) jStart = rows + jStart;
		if (iStart < 0) iStart = cols + iStart;

		for (let jOff = 0; jOff < 3; jOff += 1) {
			let curJ = (jStart + jOff) % rows;

			for (let iOff = 0; iOff < 3; iOff += 1) {
				if (stateGrid[curJ][(iStart + iOff) % cols]) neighbours += 1;
			}
		}

		if (stateGrid[j][i]) neighbours -= 1;  // Substract local cell if it was counted as a neighbour

		return neighbours;
	}
}


/* function fillUIGrid(uIGrid, stateGrid)
 * Updates @uIGrid to represent the data in @stateGrid.
 */
function fillUIGrid(uIGrid, stateGrid) {
	let rows = uIGrid.getElementsByTagName("tr");

	for (let j = 0; j < rows.length; j += 1) {
		let columns = rows[j].getElementsByTagName("td");

		for (let i = 0; i < columns.length; i += 1) {
			let checkbox = columns[i].firstChild;  // The first child of each <td> element is an <input type="checkbox"> as created by createUIGrid
			stateGrid[j][i] ? checkbox.checked = true :
			                  checkbox.checked = false;
		}
	}
}


/* doNextCycle(uIGrid, wrapAround)
 * Update @uIGrid to represent the next cycle in the Game of Life.
 */
function doNextCycle(uIGrid, wrapAround) {
	fillUIGrid(uIGrid, simulateLife(generateStateFromUI(uIGrid), wrapAround));
}


/* User Interface */

const defaultRows = 10;
const defaultColumns = 10;
const maxRows = 64;
const maxColumns = 64;
// NOTE: Minimum number of rows and columns is set on the HTML file because it
// is not meant to be changed by code.

let gRows = defaultRows;        // Current grid rows
let gColumns = defaultColumns;  // Current grid columns

let gUIGrid = replaceUIGrid(gRows, gColumns);  // Grid (uIGrid) object


function replaceUIGrid(rows, columns) {
	let uIGrid = createUIGrid(rows, columns);

	fillUIGrid(uIGrid, generateStateRandom(rows, columns));
	uIGrid.setAttribute("data-rows", rows);
	uIGrid.setAttribute("data-cols", columns);
	document.body.replaceChild(uIGrid, document.querySelector("#grid"));
	uIGrid.id = "grid";

	return uIGrid;
}


let nextCycleButton = document.querySelector("#control-next");
let wrapAroundControl = document.querySelector("#control-wrap");

nextCycleButton.addEventListener("click", () => {
	doNextCycle(gUIGrid, wrapAroundControl.checked);
});

document.querySelector("#control-clear").addEventListener("click", () => {
	fillUIGrid(gUIGrid, generateStateClear(gRows, gColumns));
	stopAutomaticMode();
});


let restartButton = document.querySelector("#control-restart");
let widthControl = document.querySelector("#control-width");
let heightControl = document.querySelector("#control-height");

widthControl.max = maxColumns;
heightControl.max = maxRows;
widthControl.value = defaultColumns;
heightControl.value = defaultRows;
widthControl.setAttribute("data-default", defaultColumns);
heightControl.setAttribute("data-default", defaultRows);

restartButton.addEventListener("click", () => {
	gRows = heightControl.value;
	gColumns = widthControl.value;
	if (gUIGrid.dataset.rows == gRows && gUIGrid.dataset.cols == gColumns) {
		fillUIGrid(gUIGrid, generateStateRandom(gRows, gColumns));
		return;
	}
	gUIGrid = replaceUIGrid(gRows, gColumns);
	restartButton.style = "";
});

widthControl.addEventListener("change", () => sizeChangeAction(widthControl));
heightControl.addEventListener("change", () => sizeChangeAction(heightControl));


function sizeChangeAction(control) {
	control.value = getControlValue(control);
	// Alert user restart is required in order to use the new size
	restartButton.style = "";
	if (heightControl.value != gRows || widthControl.value != gColumns) {
		restartButton.style = "color: red";
	}
}

function getControlValue(control) {
	if (isNaN(control.valueAsNumber)) return control.dataset.default;
	if (control.valueAsNumber < control.min) return control.min;
	if (control.valueAsNumber > control.max) return control.max;
	return control.value;
}


/* Automatic mode controls and behavior */

let automaticModeSwitch = document.querySelector("#control-automatic");
let speedControl = document.querySelector("#control-speed");

let animationID;                      // Will hold the values returned by requestAnimationFrame
let speedStep = 0;                    // Value updated each animation frame
let speedGauge = speedControl.value;  // Number of animation frames between each grid update

automaticModeSwitch.checked = false;

automaticModeSwitch.addEventListener("change", () => {
	if (automaticModeSwitch.checked) {
		nextCycleButton.disabled = true;
		animationID = requestAnimationFrame(automatic);
		return;
	}
	stopAutomaticMode();
});

speedControl.addEventListener("change", () => {
	speedGauge = speedControl.value;
});


function stopAutomaticMode() {
	cancelAnimationFrame(animationID);
	nextCycleButton.disabled = false;
	automaticModeSwitch.checked = false;
}


function automatic() {
	if (speedStep % speedGauge == 0) {
		doNextCycle(gUIGrid, wrapAroundControl.checked);
	}

	speedStep += 1;
	animationID = requestAnimationFrame(automatic);
}
